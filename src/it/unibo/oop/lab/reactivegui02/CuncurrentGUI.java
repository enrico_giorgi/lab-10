package it.unibo.oop.lab.reactivegui02;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.awt.*;
import javax.swing.*;

import it.unibo.oop.lab.reactivegui01.ConcurrentGUI;
//import it.unibo.oop.lab.reactivegui01.ConcurrentGUI.Agent;

/*
 * Realizzare una classe ConcurrentGUI con costruttore privo di argomenti,
 * tale che quando istanziata crei un JFrame con l'aspetto mostrato nella
 * figura allegata (e contatore inizialmente posto a zero).
 * 
 * Il contatore venga aggiornato incrementandolo ogni 100 millisecondi
 * circa, e il suo nuovo valore venga mostrato ogni volta (l'interfaccia sia
 * quindi reattiva).
 * 
 * Alla pressione del pulsante "down", il conteggio venga da lì in poi
 * aggiornato decrementandolo; alla pressione del pulsante "up", il
 * conteggio venga da lì in poi aggiornato incrementandolo; e così via, in
 * modo alternato.
 * 
 * Alla pressione del pulsante "stop", il conteggio si blocchi, e i tre
 * pulsanti vengano disabilitati. Per far partire l'applicazioni si tolga il
 * commento nel main qui sotto.
 * 
 * Suggerimenti: - si mantenga la struttura dell'esercizio precedente - per
 * pilotare la direzione su/gi� si aggiunga un flag booleano all'agente --
 * deve essere volatile? - la disabilitazione dei pulsanti sia realizzata
 * col metodo setEnabled
 */

public final class CuncurrentGUI extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    enum  Count {UP, DOWN};
    private final JLabel display = new JLabel();
    private final JButton stop= new JButton("stop");
    private final JButton up= new JButton("up");
    private final JButton down= new JButton("down");
    
    /**
     * build GUI
     * @param <Agent>
     */
    public CuncurrentGUI() {
        super();
        final Dimension screenSize= Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth()*WIDTH_PERC), (int) (screenSize.getHeight()*HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();    
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        this.getContentPane().add(panel);
        this.setVisible(true);
        
        final Agent agent = new Agent();
        new Thread(agent).start();
        
        
        up.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                // TODO Auto-generated method stub
                agent.upCounting();
            }
                      
        });
        down.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                // TODO Auto-generated method stub
                agent.reverseCounting();
            }
                      
        });
        stop.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                // TODO Auto-generated method stub
                agent.stopCounting();
                stop.setEnabled(false);
                up.setEnabled(false);
                down.setEnabled(false);
            }
                      
        });
        
        
    }
    private class Agent implements Runnable {
        /*
         * stop is volatile to ensure ordered access
         */
        private volatile boolean stop; //� a false
        //private volatile boolean count2=true; //false
        private int counter;
        private volatile Count count = Count.UP;

        public void run() {
            while (!this.stop) { //finch� � false continua, a true esci
                try {
                    /*
                     * All the operations on the GUI must be performed by the
                     * Event-Dispatch Thread (EDT)!
                     */
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            CuncurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    if (count == Count.UP) {
                        this.counter++;
                    } else if (count == Count.DOWN) {
                        this.counter--;
                    }
                   // counter += count2 ? 1: -1; count2 deve essere boolean
                    Thread.sleep(100);
                } catch (InvocationTargetException | InterruptedException ex) {
                    /*
                     * This is just a stack trace print, in a real program there
                     * should be some logging and decent error reporting
                     */
                    ex.printStackTrace();
                }
            }
        }

        /**
         * External command to stop counting.
         */
        public void stopCounting() {
            this.stop = true;
        }
        public void upCounting() {
            this.count = Count.UP;
        }
        public void reverseCounting() {
            this.count = Count.DOWN;
        }
    }

}
